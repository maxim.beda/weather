import { useEffect } from 'react';

// Components
import { Filters } from './components/Filters.tsx';
import { Head } from './components/Head.tsx';
import { Weather } from './components/Weather.tsx';
import { Forecast } from './components/Forecast.tsx';

// Instruments
import { useQuery } from "react-query";
import {api} from './api';

// redux
import { useSelector, useDispatch } from 'react-redux';
const selector = (state) => {
    return state.selectedDay.activeDay;
}

export const App = () => {
    console.log('component: App');

    // data load
    const { loading, error, data } = useQuery(
        "getWeather", 
        () => api.getWeather().
        then(res => res)
    );

    // redux
    const dispatch = useDispatch();
    const activeDay = useSelector(selector);

    const setActiveDay = (i) => {
        dispatch({type: "changeDay", payload: i});
        console.log('activeDay', activeDay);
    }

    if (loading) return "Loading...";
    if (error) return "Error...";

    if(!data) {
        console.log("no data");
        return "Waiting...";
    }
    
    return (
        <main>
            <Filters />
            <Head activeDay={data[activeDay]} />
            <Weather activeDay={data[activeDay]} />
            <Forecast days={data} activeDay={data[activeDay]} setActiveDay={setActiveDay} />
        </main>
    );
};

