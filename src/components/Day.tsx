// @ts-nocheck
import { format } from 'date-fns';

export const Day = (props) => {
    console.log('component: Day');

    const { id, day, type, temperature, index, setActiveDay, activeDay } = props;
    const weekDay = format(day, 'eeee');

    return (
        <div className={ `day ${type} ${activeDay.id === id && 'selected'}` } onClick = { () => setActiveDay(index) }>
            <p>{ weekDay }</p>
            <span>{ temperature }</span>
        </div>
    );
};
