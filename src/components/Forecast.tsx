// @ts-nocheck
import { Day } from './Day';

export const Forecast = (props) => {
    console.log('component: Forecast');

    const { days, activeDay, setActiveDay } = props;

    const daysJSX = days.map(
        (day, i) => 
        i < 7 && 
        <Day key={day.id} index={i} activeDay={activeDay} setActiveDay={setActiveDay} { ...day } />
    );

    return (
        <div className="forecast">
            { daysJSX }
        </div>
    );
};
