// @ts-nocheck
export const Weather = (props) => {
    console.log('component: Weather');
    console.log('Weather props: ', props);

    const { temperature, rain_probability, humidity } = props.activeDay;
    
    return (
        <div className="current-weather">
            <p className="temperature">{temperature}</p>
            <p className="meta">
                <span className="rainy">%{rain_probability}</span>
                <span className="humidity">%{humidity}</span>
            </p>
        </div>
    );
};
