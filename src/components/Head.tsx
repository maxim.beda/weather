// @ts-nocheck
import { format } from 'date-fns';

export const Head = (props) => {
    console.log('component: Head');
    console.log('Filters props: ', props);

    const { day } = props.activeDay;
    const weekDay = format(day, 'eeee');
    const monthDay = format(day, 'd');
    const monthName = format(day, 'MMMM');

    return (
        <div className="head">
            <div className="icon sunny"></div>
            <div className="current-date">
                <p>{weekDay}</p><span>{monthDay} {monthName}</span>
            </div>
        </div>
    );
};
