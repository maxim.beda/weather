const initialState = {
    activeDay: 0,
};

export const selectedDayReducer = (state = initialState, action) => {
    if (action.type === 'changeDay') {
        return {
            ...state,
            activeDay: action.payload,
        };
    }

    return state;
};
