// Core
import { combineReducers } from 'redux';

// Reducers
import { selectedDayReducer } from '../reducers/selectedDay';

export const rootReducer = combineReducers({
    selectedDay: selectedDayReducer,
});
